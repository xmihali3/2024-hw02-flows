package cz.muni.fi.pb162.hw02.impl.flowsources;

import cz.muni.fi.pb162.hw02.FlowSource;

import java.util.Iterator;

/**
 * Implementation of FlowSource using an Iterator.
 *
 * @param <T> type of elements produced by this FlowSource
 * @author Jakub Mihálik
 */
public class IteratorFlowSource<T> implements FlowSource<T> {

    private final Iterator<T> iterator;
    private T current;

    /**
     * Creates an Iterator flow source
     *
     * @param iterator iterator used as the flow source
     */
    public IteratorFlowSource(Iterator<T> iterator) {
        this.iterator = iterator;
        this.current = null;
    }

    @Override
    public boolean advance() {
        if (iterator.hasNext()) {
            current = iterator.next();
            return true;
        }
        return false;
    }

    @Override
    public T get() {
        return current;
    }
}

package cz.muni.fi.pb162.hw02.impl.flowsources;

import cz.muni.fi.pb162.hw02.FlowSource;

import java.util.function.Supplier;

/**
 * Implementation of FlowSource using a Supplier.
 *
 * @param <T> type of elements produced by this FlowSource
 * @author Jakub Mihálik
 */
public class SupplierFlowSource<T> implements FlowSource<T> {

    private final Supplier<T> supplier;
    private T current;

    /**
     * Creates Supplier flow source
     *
     * @param supplier supplier used for the flow source
     */
    public SupplierFlowSource(Supplier<T> supplier) {
        this.supplier = supplier;
        this.current = null;
    }

    @Override
    public boolean advance() {
        current = supplier.get();
        return current != null;
    }

    @Override
    public T get() {
        return current;
    }
}

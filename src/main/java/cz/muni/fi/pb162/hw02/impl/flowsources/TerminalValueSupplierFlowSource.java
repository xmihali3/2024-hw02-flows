package cz.muni.fi.pb162.hw02.impl.flowsources;

import java.util.function.Supplier;

/**
 * Extension of SupplierFlowSource using a terminal value.
 *
 * @param <T> type of elements produced by this FlowSource
 * @author Jakub Mihálik
 */
public class TerminalValueSupplierFlowSource<T> extends SupplierFlowSource<T>{
    private final T terminalValue;

    /**
     * Creates Supplier flow source with a terminal value
     *
     * @param supplier supplier of the flow source
     * @param terminalValue terminal value of the flow source
     */
    public TerminalValueSupplierFlowSource(Supplier<T> supplier, T terminalValue) {
        super(supplier);
        this.terminalValue = terminalValue;
    }

    @Override
    public boolean advance() {
        return super.advance() && (terminalValue == null || !terminalValue.equals(get()));
    }
}

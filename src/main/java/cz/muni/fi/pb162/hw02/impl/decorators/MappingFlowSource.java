package cz.muni.fi.pb162.hw02.impl.decorators;

import cz.muni.fi.pb162.hw02.FlowSource;

import java.util.function.Function;

/**
 * FlowSource implementation that maps elements using a Function.
 *
 * @param <T> type of elements produced by this FlowSource
 * @param <R> type of elements produced by the mapped FlowSource
 * @author Jakub Mihálik
 */
public class MappingFlowSource<T, R> implements FlowSource<R> {

    private final FlowSource<T> source;
    private final Function<T, R> mapper;

    /**
     * Creates a flow source mapper
     *
     * @param source flow source
     * @param mapper mapper for the flow source
     */
    public MappingFlowSource(FlowSource<T> source, Function<T, R> mapper) {
        this.source = source;
        this.mapper = mapper;
    }

    @Override
    public boolean advance() {
        return source.advance();
    }

    @Override
    public R get() {
        return mapper.apply(source.get());
    }
}

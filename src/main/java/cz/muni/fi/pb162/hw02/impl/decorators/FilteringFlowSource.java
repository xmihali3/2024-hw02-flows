package cz.muni.fi.pb162.hw02.impl.decorators;

import cz.muni.fi.pb162.hw02.FlowSource;

import java.util.function.Predicate;

/**
 * FlowSource implementation that filters elements based on a Predicate.
 *
 * @param <T> type of elements produced by this FlowSource
 * @author Jakub Mihálik
 */
public class FilteringFlowSource<T> implements FlowSource<T> {

    private final FlowSource<T> source;
    private final Predicate<T> predicate;

    /**
     * Creates a flow source filter
     *
     * @param source flow source
     * @param predicate predicate used by the filter
     */
    public FilteringFlowSource(FlowSource<T> source, Predicate<T> predicate) {
        this.source = source;
        this.predicate = predicate;
    }

    @Override
    public boolean advance() {
        while (source.advance()) {
            if (predicate.test(source.get())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public T get() {
        return source.get();
    }
}

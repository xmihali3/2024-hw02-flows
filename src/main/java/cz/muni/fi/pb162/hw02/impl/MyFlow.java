package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.Flow;
import cz.muni.fi.pb162.hw02.FlowSource;
import cz.muni.fi.pb162.hw02.impl.decorators.FilteringFlowSource;
import cz.muni.fi.pb162.hw02.impl.decorators.LimitingFlowSource;
import cz.muni.fi.pb162.hw02.impl.decorators.MappingFlowSource;
import cz.muni.fi.pb162.hw02.impl.decorators.SkippingFlowSource;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Implementation of Flow interface.
 *
 * @param <T> type of elements produced by this Flow
 * @author Jakub Mihálik
 */
public class MyFlow<T> implements Flow<T> {

    private final FlowSource<T> source;

    /**
     * Creates new flow
     *
     * @param source flow source
     */
    public MyFlow(FlowSource<T> source) {
        this.source = source;
    }

    @Override
    public Flow<T> filter(Predicate<T> filter) {
        return new MyFlow<>(new FilteringFlowSource<>(source, filter));
    }

    @Override
    public <R> Flow<R> map(Function<T, R> transformation) {
        return new MyFlow<>(new MappingFlowSource<>(source, transformation));
    }

    @Override
    public Flow<T> limit(long count) {
        return new MyFlow<>(new LimitingFlowSource<>(source, count));
    }

    @Override
    public Flow<T> skip(long count) {
        return new MyFlow<>(new SkippingFlowSource<>(source, count));
    }

    @Override
    public void forEach(Consumer<T> operation) {
        while (source.advance()) {
            T element = source.get();
            operation.accept(element);
        }
    }

    @Override
    public T first() {
        if (source.advance()) {
            return source.get();
        }
        return null;
    }
}

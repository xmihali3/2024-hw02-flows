package cz.muni.fi.pb162.hw02.impl.decorators;

import cz.muni.fi.pb162.hw02.FlowSource;

/**
 * FlowSource implementation that skips the first N elements.
 *
 * @param <T> type of elements produced by this FlowSource
 * @author Jakub Mihálik
 */
public class SkippingFlowSource<T> implements FlowSource<T> {

    private final FlowSource<T> source;
    private final long toSkip;
    private long skippedCount;

    /**
     * Creates float source skipper
     *
     * @param source flow source
     * @param toSkip number of elements to skip
     */
    public SkippingFlowSource(FlowSource<T> source, long toSkip) {
        this.source = source;
        this.toSkip = toSkip;
        this.skippedCount = 0;
    }

    @Override
    public boolean advance() {
        while (skippedCount < toSkip && source.advance()) {
            skippedCount++;
        }
        return source.advance();
    }

    @Override
    public T get() {
        return source.get();
    }
}

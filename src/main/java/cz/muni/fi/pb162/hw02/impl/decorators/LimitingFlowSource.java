package cz.muni.fi.pb162.hw02.impl.decorators;

import cz.muni.fi.pb162.hw02.FlowSource;

/**
 * FlowSource implementation that limits the number of elements produced.
 *
 * @param <T> type of elements produced by this FlowSource
 * @author Jakub Mihálik
 */
public class LimitingFlowSource<T> implements FlowSource<T> {

    private final FlowSource<T> source;
    private final long limit;
    private long count;

    /**
     * Creates flow source limiter
     *
     * @param source flow source
     * @param limit the limit of the flow source
     */
    public LimitingFlowSource(FlowSource<T> source, long limit) {
        this.source = source;
        this.limit = limit;
        this.count = 0;
    }

    @Override
    public boolean advance() {
        if (count < limit && source.advance()) {
            count++;
            return true;
        }
        return false;
    }

    @Override
    public T get() {
        return source.get();
    }
}
